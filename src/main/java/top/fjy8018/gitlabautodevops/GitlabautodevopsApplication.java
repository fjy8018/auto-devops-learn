package top.fjy8018.gitlabautodevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabautodevopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabautodevopsApplication.class, args);
    }

}
